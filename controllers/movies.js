const express = require('express')
const Movie = require('../models/movie');

function list(req,res,next){
    let page = req.params.page ? req.params.page : 1;
    // e.g. 3 pags con 7 documentos
    Movie.paginate({},{page:page, limit:3, populate:'_actors'}).then(objs => res.status(200).json({
        message: res.__('ok.moviesList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.moviesList'),
        obj: ex
    }));
}

function index(req,res,next){
    const id = req.params.id;
    Movie.findOne({"_id":id}).populate('_actors').then(obj => res.status(200).json({
        message: res.__('ok.moviesIndex') + `${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.moviesIndex') + `${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    
    const director = {
        name:req.body.directorName,
        lastName:req.body.directorLastName,
    }

    const actors = req.body.actorId;
    const genre = req.body.genre;
    const title = req.body.title;

    let movie = new Movie({
        director:director,
        actors:actors,
        genre:genre,
        title:title,
    });

    movie.save().then(obj => res.status(200).json({
        message: res.__('ok.moviesCreate'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.moviesCreate'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    
    let director = {
        name:req.body.directorName ? req.body.directorName: "",
        lastName:req.body.directorLastName ? req.body.directorLastName:"",
    }
    let actors = req.body.actorId ? req.body.actorId: null;
    let genre = req.body.genre ? req.body.genre: "";
    let title = req.body.title ? req.body.title: "";

    let movie = new Object({
        director:director,
        actors:actors,
        genre:genre,
        title:title,
    });

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
        message: res.__('ok.moviesReplace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.moviesReplace'),
        obj:ex
    }));
}

function edit(req, res, next){
    let id = req.params.id;

    let director = {
        name:req.body.directorName,
        lastName:req.body.directorLastName,
    }
    const actors = req.body.actorId;
    let genre = req.body.genre;
    let title = req.body.title;

    let movie = new Object();

    if(director){
        movie._director = director;
    }
    if(actors){
        movie._actors = actors;
    }
    if(genre){
        movie._genre = genre;
    }
    if(title){
        movie._title = title;
    }

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj=>res.status(200).json({
        message: res.__('ok.moviesEdit'),
        obj:obj
    })).catch(ex=>res.status(500).json({
        message: res.__('bad.moviesEdit'),
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.moviesDestroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.moviesDestroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}