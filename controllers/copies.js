const express = require('express')
const Copy = require('../models/copy');

function list(req,res,next){
    Copy.find().populate('_movie').then(objs => res.status(200).json({
        message: res.__('ok.copiesList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.copiesList'),
        obj: ex
    }));
}

function index(req,res,next){
    const id = req.params.id;
    Copy.findOne({"_id":id}).populate('_movie').then(obj => res.status(200).json({
        message: res.__('ok.copiesIndex') + `${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.copiesIndex') + `${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    const format = req.body.format;
    const movie = req.body.movieId;
    const number = req.body.number;
    const status = req.body.status;

    let copy = new Copy({
        format:format,
        movie:movie,
        number:number,
        status:status
    });

    copy.save().then(obj => res.status(200).json({
        message: res.__('ok.copiesCreate'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.copiesCreate'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let format = req.body.format ? req.body.format: "";
    let movie = req.body.movieId ? req.body.movieId: null;
    let number = req.body.number ? req.body.number: 0;
    let status = req.body.status ? req.body.status: "";

    let copy = new Object({
        format:format,
        movie:movie,
        number:number,
        status:status
    });

    Copy.findOneAndUpdate({"_id":id}, copy).then(obj => res.status(200).json({
        message: res.__('ok.copiesReplace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.copiesReplace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let format = req.body.format;
    let movie = req.body.movieId;
    let number = req.body.number;
    let status = req.body.status;

    let copy = new Object();

    if(format){
        copy._format = format;
    }
    if(movie){
        copy._movie = movie;
    }
    if(number){
        copy._number = number;
    }
    if(status){
        copy._status = status;
    }

    Copy.findOneAndUpdate({"_id":id}, copy).then(obj=>res.status(200).json({
        message: res.__('ok.copiesEdit'),
        obj:obj
    })).catch(ex=>res.status(500).json({
        message: res.__('bad.copiesEdit'),
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.copiesDestroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.copiesDestroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}