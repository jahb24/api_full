const express = require('express')
const Member = require('../models/member');

function list(req,res,next){
    let page = req.params.page ? req.params.page : 1;
    // e.g. 3 pags con 7 documentos
    Member.paginate({},{page:page, limit:3}).then(objs => res.status(200).json({
        message: res.__('ok.membersList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.membersList'),
        obj: ex
    }));
}

function index(req,res,next){
    const id = req.params.id;
    Member.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.membersIndex') + `${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.membersIndex') + `${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    
    const address = {
        _city:req.body.city,
        _country:req.body.country,
        _number:req.body.number,
        _state:req.body.state,
        _street:req.body.street
    }
    const lastName = req.body.lastName;
    const name = req.body.name;
    const phone = req.body.phone;
    const status = req.body.status;

    let member = new Member({
        address:address,
        lastName:lastName,
        name:name,
        phone:phone,
        status:status
    });

    member.save().then(obj => res.status(200).json({
        message: res.__('ok.membersCreate'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.membersCreate'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    
    let address = {
        _city:req.body.city ? req.body.city: "",
        _country:req.body.country ? req.body.country:"",
        _number:req.body.number ? req.body.number: "",
        _state:req.body.state ? req.body.state: "",
        _street:req.body.street ? req.body.street: ""
    }
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let phone = req.body.phone ? req.body.phone: "";
    let status = req.body.status ? req.body.status: false;

    let member = new Object({
        address:address,
        name:name,
        lastName:lastName,
        phone:phone,
        status:status
    });

    Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
        message: res.__('ok.membersReplace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.membersReplace'),
        obj:ex
    }));
}

function edit(req, res, next){
    let id = req.params.id;

    const address = {
        _city:req.body.city,
        _country:req.body.country,
        _number:req.body.number,
        _state:req.body.state,
        _street:req.body.street
    }
    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;

    let member = new Object();

    if(address){
        member._address = address;
    }
    if(name){
        member._name = name;
    }
    if(lastName){
        member._lastName = lastName;
    }
    if(phone){
        member._phone = phone;
    }
    if(status){
        member._status = status;
    }

    Member.findOneAndUpdate({"_id":id}, member).then(obj=>res.status(200).json({
        message: res.__('ok.membersEdit'),
        obj:obj
    })).catch(ex=>res.status(500).json({
        message: res.__('bad.membersEdit'),
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.membersDestroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.membersDestroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}