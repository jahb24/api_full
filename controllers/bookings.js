const express = require('express')
const Booking = require('../models/booking');

function list(req,res,next){
    Booking.find().populate('_member _copy').then(objs => res.status(200).json({
        message: res.__('ok.bookingsList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.bookingsList'),
        obj: ex
    }));
}

function index(req,res,next){
    const id = req.params.id;
    Booking.findOne({"_id":id}).populate('_member _copy').then(obj => res.status(200).json({
        message: res.__('ok.bookingsIndex') + `${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.bookingsIndex') + `${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    const copy = req.body.copyId;
    const member = req.body.memberId;
    const date = req.body.date;

    let booking = new Booking({
        copy:copy,
        member:member,
        date:date
    });

    booking.save().then(obj => res.status(200).json({
        message: res.__('ok.bookingsCreate'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.bookingsCreate'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let copy = req.body.copy ? req.body.copyId: null;
    let member = req.body.memberId ? req.body.memberId: null;
    let date = req.body.date ? req.body.date: null;

    let booking = new Object({
        copy:copy,
        member:member,
        date:date,
    });

    Booking.findOneAndUpdate({"_id":id}, booking).then(obj => res.status(200).json({
        message: res.__('ok.bookingsReplace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.bookingsReplace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let copy = req.body.copyId;
    let member = req.body.memberId;
    let date = req.body.date;

    let booking = new Object();

    if(copy){
        booking._copy = copy;
    }
    if(member){
        booking._member = member;
    }
    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id}, booking).then(obj=>res.status(200).json({
        message: res.__('ok.bookingsEdit'),
        obj:obj
    })).catch(ex=>res.status(500).json({
        message: res.__('bad.bookingsEdit'),
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.bookingsDestroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.bookingsDestroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}