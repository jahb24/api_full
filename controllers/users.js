const express = require('express')
const bcrypt = require('bcrypt');
const async = require('async');
const User = require('../models/user');
//const acl = require('acl');

function list(req,res,next){
    
    let page = req.params.page ? req.params.page : 1;
    // e.g. 3 pags con 7 documentos
    User.paginate({},{page:page, limit:3}).then(objs => res.status(200).json({
        message: res.__('ok.usersList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersList'),
        obj: ex
    }));
}

function index(req,res,next){
    const id = req.params.id;
    User.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.usersIndex') + `${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersIndex') + `${id}`,
        obj: ex
    }));
}

function create(req,res,next){
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    /*const rol = req.body.rol;
    const permission = {
        _description:req.body.description
    }*/
    async.parallel({
        salt:(callback)=>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) => {
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new User({
                _name:name,
                _lastName:lastName,
                _email:email,
                _password:hash,
                _salt:result.salt,
                /*_rol:rol,
                _permission:permission*/
            });
            //acl.addUserRoles(req.body.name, rol);
            user.save().then(obj => res.status(200).json({
                message: res.__('ok.usersCreate'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.usersCreate'),
                obj:ex
            }));
        })
    });
}

function replace(req, res, next){
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let email = req.body.email ? req.body.email: "";
    let password = req.body.password;

    async.parallel({
        salt:(callback)=>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) => {
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new Object({
                _name:name,
                _lastName:lastName,
                _email:email,
                _password:hash,
                _salt:result.salt,
            });
            
            User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
                message: res.__('ok.usersReplace'),
                obj:obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.usersReplace'),
                obj:ex
            }));
        })
    });
    
}

function edit(req, res, next){
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let email = req.body.email;
    let password = req.body.password;
    async.parallel({
        salt:(callback)=>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) => {
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new Object({
                _password:hash,
                _salt:result.salt,
            });
            if(name){
                user._name = name;
            }
            if(lastName){
                user._lastName = lastName;
            }
            if(email){
                user._email = email;
            }
            User.findOneAndUpdate({"_id":id}, user).then(obj=>res.status(200).json({
                message: res.__('ok.usersEdit'),
                obj:obj
            })).catch(ex=>res.status(500).json({
                message: res.__('bad.usersEdit'),
                obj:ex
            }));
        })
    });
}

function destroy(req, res, next){
    const id = req.params.id;
    User.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.usersDestroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersDestroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}