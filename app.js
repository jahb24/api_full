var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const expressjwt = require('express-jwt');
const config = require('config');
const i18n = require('i18n');

//var node_acl = require('acl');
//let acl = null;

const indexRouter = require('./routes/index');
const actorsRouter = require('./routes/actors');
const usersRouter = require('./routes/users');
const membersRouter = require('./routes/members');
const moviesRouter = require('./routes/movies');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');

//"mongodb://<dbuser>?:<dbPassword>?@<direction>:<port>/<dbName>"
const jwtKey = config.get("secret.key");
//console.log(jwtKey);

const uri = config.get("dbChain");
mongoose.connect(uri);/*, (error, db)=>{
  if(error){
    throw error;
  }
  acl = new node_acl.mongodbBackend(db, '_acl');
});*/


const db = mongoose.connection;
const app = express();

db.on('error', ()=>{
  console.log("No se ha podido conectar");
});
db.on('open', ()=>{
  console.log("quedó prron");
});

i18n.configure({
  locales:['es', 'en'],
  cookie:'language',
  directory:`${__dirname}/locales`
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//middleware static => define donde se encuentran todos los recursos estaticos de la app
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init)

app.use(expressjwt({secret:jwtKey, algorithms:['HS256']})
.unless({path:["/login"]}))

app.use('/actors', actorsRouter);
app.use('/users', usersRouter);
app.use('/', indexRouter);
app.use('/members', membersRouter);
app.use('/movies', moviesRouter);
app.use('/copies', copiesRouter);
app.use('/bookings', bookingsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
