const supertest = require('supertest');

const app = require('../app');

var key = "";

//sentencia
describe('probar sistema de autenticación', ()=>{
    //casos de prueba => 50%
    it('debería obtener un login con usuario y contraseña correctos', (done)=>{
        supertest(app).post('/login')
        .send({'email':'a12@uach.mx', 'password':'1234'})
        .expect(200) //status HTTP code
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de los usuarios', ()=>{
    it('deberia obtener la lista de usuarios', (done)=>{
      supertest(app).get('/users/')
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
        if(err){
          done(err);
        }else{
          expect(res.statusCode).toEqual(200);
          done();
        }
      });
    });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia obtener un usuario', (done)=>{
    supertest(app).get('/users/show/619d7144ee4d0278002c6637')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia crear un usuario', (done)=>{
    supertest(app).post('/users')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia reemplazar un usuario', (done)=>{
    supertest(app).put('/users/619d7144ee4d0278002c6637')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia editar un usuario', (done)=>{
    supertest(app).patch('/users/619d7144ee4d0278002c6637')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia eliminar un usuario', (done)=>{
    supertest(app).delete('/users/619d7144ee4d0278002c6637')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});