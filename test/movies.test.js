const supertest = require('supertest');

const app = require('../app');

var key = "";

//sentencia
describe('probar sistema de autenticación', ()=>{
    //casos de prueba => 50%
    it('debería obtener un login con usuario y contraseña correctos', (done)=>{
        supertest(app).post('/login')
        .send({'email':'a12@uach.mx', 'password':'1234'})
        .expect(200) //status HTTP code
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de las peliculas', ()=>{
    it('deberia obtener la lista de peliculas', (done)=>{
      supertest(app).get('/movies/')
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
        if(err){
          done(err);
        }else{
          expect(res.statusCode).toEqual(200);
          done();
        }
      });
    });
});

describe('probar las rutas de las peliculas', ()=>{
  it('deberia obtener una pelicula', (done)=>{
    supertest(app).get('/movies/show/619d71f7ee4d0278002c6639')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las peliculas', ()=>{
  it('deberia crear una pelicula', (done)=>{
    supertest(app).post('/movies')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las peliculas', ()=>{
  it('deberia reemplazar una pelicula', (done)=>{
    supertest(app).put('/movies/619d71f7ee4d0278002c6639')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las peliculas', ()=>{
  it('deberia editar una pelicula', (done)=>{
    supertest(app).patch('/movies/619d71f7ee4d0278002c6639')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las peliculas', ()=>{
  it('deberia eliminar una pelicula', (done)=>{
    supertest(app).delete('/movies/619d71f7ee4d0278002c6639')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});