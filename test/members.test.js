const supertest = require('supertest');

const app = require('../app');

var key = "";

//sentencia
describe('probar sistema de autenticación', ()=>{
    //casos de prueba => 50%
    it('debería obtener un login con usuario y contraseña correctos', (done)=>{
        supertest(app).post('/login')
        .send({'email':'a12@uach.mx', 'password':'1234'})
        .expect(200) //status HTTP code
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de los miembros', ()=>{
    it('deberia obtener la lista de miembros', (done)=>{
      supertest(app).get('/members/')
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
        if(err){
          done(err);
        }else{
          expect(res.statusCode).toEqual(200);
          done();
        }
      });
    });
});

describe('probar las rutas de los miembros', ()=>{
  it('deberia obtener un miembro', (done)=>{
    supertest(app).get('/members/show/619da3705c58ff90a788cbfe')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los miembros', ()=>{
  it('deberia crear un miembro', (done)=>{
    supertest(app).post('/members')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los miembros', ()=>{
  it('deberia reemplazar un miembro', (done)=>{
    supertest(app).put('/members/619da3705c58ff90a788cbfe')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los miembros', ()=>{
  it('deberia editar un miembro', (done)=>{
    supertest(app).patch('/members/619da3705c58ff90a788cbfe')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de los miembros', ()=>{
  it('deberia eliminar un miembro', (done)=>{
    supertest(app).delete('/members/619da3705c58ff90a788cbfe')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});