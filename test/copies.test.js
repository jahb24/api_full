const supertest = require('supertest');

const app = require('../app');

var key = "";

//sentencia
describe('probar sistema de autenticación', ()=>{
    //casos de prueba => 50%
    it('debería obtener un login con usuario y contraseña correctos', (done)=>{
        supertest(app).post('/login')
        .send({'email':'a12@uach.mx', 'password':'1234'})
        .expect(200) //status HTTP code
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de las copias', ()=>{
    it('deberia obtener la lista de copias', (done)=>{
      supertest(app).get('/copies')
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
        if(err){
          done(err);
        }else{
          expect(res.statusCode).toEqual(200);
          done();
        }
      });
    });
});

describe('probar las rutas de las copias', ()=>{
  it('deberia obtener una copia', (done)=>{
    supertest(app).get('/copies/619da4305c58ff90a788cc04')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las copias', ()=>{
  it('deberia crear una copia', (done)=>{
    supertest(app).post('/copies')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las copias', ()=>{
  it('deberia reemplazar una copia', (done)=>{
    supertest(app).put('/copies/619da4305c58ff90a788cc04')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las copias', ()=>{
  it('deberia editar una copia', (done)=>{
    supertest(app).patch('/copies/619da4305c58ff90a788cc04')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las copias', ()=>{
  it('deberia eliminar una copia', (done)=>{
    supertest(app).delete('/copies/619da4305c58ff90a788cc04')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});