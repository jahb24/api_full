const supertest = require('supertest');

const app = require('../app');

var key = "";

//sentencia
describe('probar sistema de autenticación', ()=>{
    //casos de prueba => 50%
    it('debería obtener un login con usuario y contraseña correctos', (done)=>{
        supertest(app).post('/login')
        .send({'email':'a12@uach.mx', 'password':'1234'})
        .expect(200) //status HTTP code
        .end(function(err, res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de las reservaciones', ()=>{
    it('deberia obtener la lista de reservaciones', (done)=>{
      supertest(app).get('/bookings')
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
        if(err){
          done(err);
        }else{
          expect(res.statusCode).toEqual(200);
          done();
        }
      });
    });
});

describe('probar las rutas de las reservaciones', ()=>{
  it('deberia obtener una reservacion', (done)=>{
    supertest(app).get('/bookings/619da4d25c58ff90a788cc0a')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las reservaciones', ()=>{
  it('deberia crear una reservacion', (done)=>{
    supertest(app).post('/bookings')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las reservaciones', ()=>{
  it('deberia reemplazar una reservacion', (done)=>{
    supertest(app).put('/bookings/619da4d25c58ff90a788cc0a')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las reservaciones', ()=>{
  it('deberia editar una reservacion', (done)=>{
    supertest(app).patch('/bookings/619da4d25c58ff90a788cc0a')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('probar las rutas de las reservaciones', ()=>{
  it('deberia eliminar una reservacion', (done)=>{
    supertest(app).delete('/bookings/619da4d25c58ff90a788cc0a')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});