//var acl = require('acl');
const express = require('express');
const router = express.Router();
//acl = new acl(new acl.memoryBackend());

/*acl.allow([
    {
        roles:'admin',
        allows:[
            {resources:'/:id', permissions:'*'}
        ]
    }
]);*/

const controller = require('../controllers/users');

/* GET users listing. => /users/ */

router.get('/:page?', controller.list);

router.get('/show/:id', controller.index);

router.post('/', controller.create);

router.put('/:id', controller.replace);

router.patch('/:id', controller.edit);

router.delete('/:id', controller.destroy);

/*router.get('/:page?', acl.middleware(), controller.list);

router.get('/show/:id', acl.middleware(), controller.index);

router.post('/', acl.middleware(), controller.create);

router.put('/:id', acl.middleware(), controller.replace);

router.patch('/:id', acl.middleware(), controller.edit);

router.delete('/:id', acl.middleware(), controller.destroy);*/

module.exports = router;